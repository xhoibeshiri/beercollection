from django.db import models

# Create your models here.


class Beer(models.Model):
    class Meta:
        verbose_name = 'Beer'
        verbose_name_plural = 'Beers'
        db_table = 'beer'

    name = models.CharField(max_length=30, blank=False, null=False)
    new_rating = models.DecimalField(max_digits=2, decimal_places=1, null=True)
    rating = models.DecimalField(max_digits=2, decimal_places=1, blank=False, null=False)
    total_ratings = models.IntegerField(default=0, blank=True, null=True)
    type = models.CharField(max_length=30, blank=False, null=False)

    def save(self, *args, **kwargs):

        if self.new_rating is not None:
            temp = self.rating * self.total_ratings
            self.total_ratings += 1
            self.rating = (temp + self.new_rating) / self.total_ratings
            super().save(*args, **kwargs)
        else:
            super().save(*args, **kwargs)

