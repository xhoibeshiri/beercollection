from django.urls import path
from .views import BeerUpdateView, BeerListCreateAPIView

urlpatterns = [
    path('list/', BeerListCreateAPIView.as_view()),
    path('update/<pk>/', BeerUpdateView.as_view())
]
