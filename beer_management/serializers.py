from rest_framework.serializers import ModelSerializer
from rest_framework.serializers import DecimalField
from django_filters import rest_framework as filters
from .models import Beer


class BeerSerializer(ModelSerializer):
    class Meta:
        model = Beer
        exclude = ['total_ratings', 'new_rating']
        extra_kwargs = {'id': {'read_only': True}}


class BeerUpdateSerializer(ModelSerializer):  # noqa
    new_rating = DecimalField(max_digits=2, decimal_places=1)

    class Meta:
        model = Beer
        fields = ['new_rating']


class BeerFilterSerializer(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')
