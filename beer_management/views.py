from rest_framework import generics
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from .models import Beer
from .serializers import BeerSerializer, BeerUpdateSerializer, BeerFilterSerializer
from django.shortcuts import get_object_or_404


# Create your views here.


class BeerListCreateAPIView(generics.ListCreateAPIView):
    queryset = Beer.objects.all()
    serializer_class = BeerSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = BeerFilterSerializer


class BeerUpdateView(generics.UpdateAPIView):
    serializer_class = BeerUpdateSerializer
    queryset = Beer.objects.all()

    def update(self, request, *args, **kwargs):
        beer = get_object_or_404(Beer, id=kwargs['pk'])
        serializer = BeerUpdateSerializer(beer, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)



