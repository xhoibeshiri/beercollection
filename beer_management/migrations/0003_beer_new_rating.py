# Generated by Django 4.2.11 on 2024-04-27 05:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('beer_management', '0002_beer_total_ratings'),
    ]

    operations = [
        migrations.AddField(
            model_name='beer',
            name='new_rating',
            field=models.DecimalField(decimal_places=1, default=3, max_digits=2),
            preserve_default=False,
        ),
    ]
